package com.example.projecttycoon

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModel
import com.example.projecttycoon.model.Money_view_model

class SecondActivity : AppCompatActivity() {
    companion object {
        const val Second = "SecondActivity"
    }




    var clickCount = 0
    var money = 0
    var Moneyclick = 1

    var cost1 = 1
    var cost2 = 2
    var cost3 = 3
    var cost4 = 4




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        Log.d(Second , "onCreate Called")



        val buttonback = findViewById<Button>(R.id.back1)
        buttonback.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }
        val buttonfashion = findViewById<Button>(R.id.fashion)
        buttonfashion.setOnClickListener {
            val intent = Intent(this, SeconAtivity2::class.java)
            startActivity(intent)

        }

//        class sharedViewModel : ViewModel() {
//            var sharedViewModel = money
//
//        }







        val buttonClick = findViewById<Button>(R.id.button_click)
        buttonClick.setOnClickListener {
            clickCount++
            val Cost = findViewById<TextView>(R.id.cost)
            val Money = findViewById<TextView>(R.id.Zero)
            Cost.text = clickCount.toString()

            if (clickCount == 5) {
                money = money + Moneyclick
                Money.text = money.toString()
                clickCount = 0

            }
            val PriceCost1 = findViewById<TextView>(R.id.cost_10)
            val money1 = findViewById<TextView>(R.id.Zero)
            val buttonClickBuy1 = findViewById<Button>(R.id.button_1)
            buttonClickBuy1.setOnClickListener {
                if (money >= 10) {
                    money = money - 10
                    money1.text = money.toString()
                    buttonClickBuy1.isVisible = false
                    PriceCost1.text = cost1.toString()
                    Moneyclick = Moneyclick + 1


                }
            }

            val PriceCost2 = findViewById<TextView>(R.id.cost_20)
            val money2 = findViewById<TextView>(R.id.Zero)
            val buttonClickBuy2 = findViewById<Button>(R.id.button_2)
            buttonClickBuy2.setOnClickListener {
                if (money >= 20) {
                    money = money - 20
                    money2.text = money.toString()
                    buttonClickBuy2.isVisible = false
                    PriceCost2.text = cost2.toString()
                    Moneyclick = Moneyclick + 2


                }
            }
            val PriceCost3 = findViewById<TextView>(R.id.cost_30)
            val money3 = findViewById<TextView>(R.id.Zero)
            val buttonClickBuy3 = findViewById<Button>(R.id.button_3)
            buttonClickBuy3.setOnClickListener {
                if (money >= 30) {
                    money = money - 20
                    money3.text = money.toString()
                    buttonClickBuy3.isVisible = false
                    PriceCost3.text = cost3.toString()
                    Moneyclick = Moneyclick + 3


                }
            }
            val PriceCost4 = findViewById<TextView>(R.id.cost_40)
            val money4 = findViewById<TextView>(R.id.Zero)
            val buttonClickBuy4 = findViewById<Button>(R.id.button_4)
            buttonClickBuy4.setOnClickListener {
                if (money >= 40) {
                    money = money - 40
                    money4.text = money.toString()
                    buttonClickBuy4.isVisible = false
                    PriceCost4.text = cost4.toString()
                    Moneyclick = Moneyclick + 4


                }
            }
        }}override fun onStart() {
            super.onStart()
            Log.d(Second, "onStart Called")
        }
    override fun onResume() {
        super.onResume()
        Log.d(Second, "onResume Called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(Second, "onPause Called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(Second, "onStop Called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(Second, "onDestroy Called")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(Second, "onRestart Called")
    }}