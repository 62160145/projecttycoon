package com.example.projecttycoon.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Money_view_model : ViewModel(){
    private val _costbox = MutableLiveData(0)
    val costbox : LiveData<Int> = _costbox
    fun setCost_box(NumberCostBox : Int) {
        _costbox.value = NumberCostBox
    }
}